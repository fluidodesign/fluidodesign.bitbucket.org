/**
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

// DO NOT EDIT THIS GENERATED OUTPUT DIRECTLY!
// This file should be overwritten as part of your build process.
// If you need to extend the behavior of the generated service worker, the best approach is to write
// additional code and include it using the importScripts option:
//   https://github.com/GoogleChrome/sw-precache#importscripts-arraystring
//
// Alternatively, it's possible to make changes to the underlying template file and then use that as the
// new base for generating output, via the templateFilePath option:
//   https://github.com/GoogleChrome/sw-precache#templatefilepath-string
//
// If you go that route, make sure that whenever you update your sw-precache dependency, you reconcile any
// changes made to this original template file with your modified copy.

// This generated service worker JavaScript will precache your site's resources.
// The code needs to be saved in a .js file at the top-level of your site, and registered
// from your pages in order to be used. See
// https://github.com/googlechrome/sw-precache/blob/master/demo/app/js/service-worker-registration.js
// for an example of how you can register this script and handle various service worker events.

/* eslint-env worker, serviceworker */
/* eslint-disable indent, no-unused-vars, no-multiple-empty-lines, max-nested-callbacks, space-before-function-paren, quotes, comma-spacing */
'use strict';

var precacheConfig = [["bower_components/app-layout/app-drawer-layout/app-drawer-layout.html","123fb24a2421ae0ffb6c5f39262799b2"],["bower_components/app-layout/app-drawer/app-drawer.html","7f453d01220ac2397d98177c281064b6"],["bower_components/app-layout/app-header-layout/app-header-layout.html","ea85181e4ef7fd8b4d354180e186deb3"],["bower_components/app-layout/app-header/app-header.html","6a044446cab0e9eecbb4686cd984486b"],["bower_components/app-layout/app-layout-behavior/app-layout-behavior.html","310c4afa57703b82569e36a85e66edb5"],["bower_components/app-layout/app-scroll-effects/app-scroll-effects-behavior.html","6496ad0cb7c5ee14066adf7ae049da38"],["bower_components/app-layout/app-scroll-effects/app-scroll-effects.html","fb2284ed40cd812eb82c44b8058111d6"],["bower_components/app-layout/app-scroll-effects/effects/blend-background.html","a2f36d0a552956bc9585e8dda0772e7c"],["bower_components/app-layout/app-scroll-effects/effects/fade-background.html","c890605e105839f2e003b0704db6c51e"],["bower_components/app-layout/app-scroll-effects/effects/material.html","22644ca27322c6849e4986c8fcb0cf63"],["bower_components/app-layout/app-scroll-effects/effects/parallax-background.html","a80dac7c479f22fec13ed71fe0457af2"],["bower_components/app-layout/app-scroll-effects/effects/resize-snapped-title.html","d516a3e0f90303070890559effe61b83"],["bower_components/app-layout/app-scroll-effects/effects/resize-title.html","00db2e77d2280832a05e414a0042d272"],["bower_components/app-layout/app-scroll-effects/effects/waterfall.html","4fa83c11e247b4a0c617973ab68d2cda"],["bower_components/app-layout/app-toolbar/app-toolbar.html","4d8b9162e3a6c5de80213ddf77b3af0b"],["bower_components/app-layout/helpers/helpers.html","0d268bf351656d21633c5c7a81e8536e"],["bower_components/app-route/app-location.html","997c1552a591c2e80e63d264778f11c3"],["bower_components/app-route/app-route-converter-behavior.html","67ec6daf2bbe9f59beecbdd5b863af14"],["bower_components/app-route/app-route.html","c9d13c7485e7e4ee09fd4c48af557167"],["bower_components/app-storage/app-localstorage/app-localstorage-document.html","ec2277443ffa25a6bb5800730c727b78"],["bower_components/app-storage/app-storage-behavior.html","fd6fa445d1dafc3cbf4b57d726b7f46f"],["bower_components/fluido-expansion-panel/fluido-expansion-icons.html","8bdc55e63c5d1c3b722a9c67e7d31620"],["bower_components/fluido-expansion-panel/fluido-expansion-panel.html","56e5cc2525a3964c8d001f35affccbc4"],["bower_components/fluido-expansion-panel/fluido-expansion-panels.html","9d7716a466d048b45f4c07e2542d4e4d"],["bower_components/fluido-paper-login/fluido-paper-login-icons.html","a317ce239731b22985beb406002fefb6"],["bower_components/fluido-paper-login/fluido-paper-login.html","7c5fcb3078ae530ec43ecf7b0abe23c4"],["bower_components/fluido-stepper/fluido-step-label.html","53fc123825bc1b85b32af536d725d4b6"],["bower_components/fluido-stepper/fluido-step.html","2408189cdba5aa81c1b5798e3b3fe01f"],["bower_components/fluido-stepper/fluido-stepper-icons.html","a2a84372655263abf7534389f14ba4d6"],["bower_components/fluido-stepper/fluido-stepper.html","a83adc7fd8b2f09dbe806a09e6ad2785"],["bower_components/font-roboto/roboto.html","3dd603efe9524a774943ee9bf2f51532"],["bower_components/google-chart/charts-loader.html","0e92e6ecf626859c15830172bcf15562"],["bower_components/google-chart/google-chart-loader.html","365f34991ce07291573d46ed7b335fef"],["bower_components/google-chart/google-chart-styles.html","0edde08b4464aaf1d7de498898f50dc6"],["bower_components/google-chart/google-chart.html","c2a62e7ed76d54a1c2a05c9abe3dc426"],["bower_components/iron-a11y-announcer/iron-a11y-announcer.html","98f9860203287b4f478cd840b537978d"],["bower_components/iron-a11y-keys-behavior/iron-a11y-keys-behavior.html","605f032cd7c8d41328244584f8070439"],["bower_components/iron-a11y-keys/iron-a11y-keys.html","89b1ca815e676d94afe2f7acdeda97eb"],["bower_components/iron-ajax/iron-ajax.html","ab41e33ccdee17382554ced56d4ef299"],["bower_components/iron-ajax/iron-request.html","e856eea9e18065772aee9cf4100e22d2"],["bower_components/iron-autogrow-textarea/iron-autogrow-textarea.html","95c79a965a4db157a45d64c62e8a438f"],["bower_components/iron-behaviors/iron-button-state.html","595db047ba4aec696c24e79d19396417"],["bower_components/iron-behaviors/iron-control-state.html","145ef28e6314796b50f6ded5c834fdfe"],["bower_components/iron-checked-element-behavior/iron-checked-element-behavior.html","8c63600df7a158047905d17db88ef1b7"],["bower_components/iron-collapse/iron-collapse.html","3c68a274784a4d784b2beb1d6609ece8"],["bower_components/iron-dropdown/iron-dropdown-scroll-manager.html","0697a9c556e58ffbbba2890c1d7eaf46"],["bower_components/iron-dropdown/iron-dropdown.html","b3a4120ebff4e112752f08bd73d4f5f4"],["bower_components/iron-fit-behavior/iron-fit-behavior.html","c743d9dd95efc32e3c75e30e722c91aa"],["bower_components/iron-flex-layout/iron-flex-layout.html","b4167b1ef8625e46afff04957be2ba63"],["bower_components/iron-form-element-behavior/iron-form-element-behavior.html","01d7748db89364c820c43ba91db4cffd"],["bower_components/iron-form/iron-form.html","334ff8e86d977f6ae173b7816d4d0f0b"],["bower_components/iron-icon/iron-icon.html","c3d6ff48c67f540e181f0c73b99adaa1"],["bower_components/iron-icons/iron-icons.html","39dd9e0a18eb2bae809992991822b048"],["bower_components/iron-iconset-svg/iron-iconset-svg.html","f691826666363bc5c15a5f645ca250c6"],["bower_components/iron-image/iron-image.html","7937e0cdc1b7f78b77136cfd2efca6f0"],["bower_components/iron-input/iron-input.html","9c3603ca6ae7de7dc05f77a165763fca"],["bower_components/iron-location/iron-location.html","b57e6be4843bfc1b61f2a9f475b09826"],["bower_components/iron-location/iron-query-params.html","a207900495d7ad3d30d039a1b7a72ab4"],["bower_components/iron-media-query/iron-media-query.html","f9986c525509dc183019cc1d331d72bb"],["bower_components/iron-menu-behavior/iron-menu-behavior.html","4e642bbe4d90c8fca1a231b8cfef661e"],["bower_components/iron-menu-behavior/iron-menubar-behavior.html","0ee53dcf429f4b54c154eab6c1ccddbc"],["bower_components/iron-meta/iron-meta.html","c97f3b7335f9a2bc06bf130afd857e6b"],["bower_components/iron-overlay-behavior/iron-focusables-helper.html","2bb54c04c009bcd05e360353347f0c61"],["bower_components/iron-overlay-behavior/iron-overlay-backdrop.html","75e5e579e50b58450e2304f4664f9c72"],["bower_components/iron-overlay-behavior/iron-overlay-behavior.html","e6beae12ea2835e38a1e17198bd8a5c6"],["bower_components/iron-overlay-behavior/iron-overlay-manager.html","6357589999cc64e12c5175c268c7fffa"],["bower_components/iron-overlay-behavior/iron-scroll-manager.html","c4e602f2d321ca77dea1772359915f85"],["bower_components/iron-pages/iron-pages.html","c077c9f5008f805b0e7bdd13a77d867c"],["bower_components/iron-range-behavior/iron-range-behavior.html","c8de798de86714942fe47e12b2be5033"],["bower_components/iron-resizable-behavior/iron-resizable-behavior.html","7b945c0343e1e62ca88ba4e41882b587"],["bower_components/iron-scroll-target-behavior/iron-scroll-target-behavior.html","8838fa134ea25d7593052f07ce8053a8"],["bower_components/iron-selector/iron-multi-selectable.html","80b186e468e6faf7c71423e0210cc4b7"],["bower_components/iron-selector/iron-selectable.html","b1b765058bd67f0d9ce8c9fab6a54e2b"],["bower_components/iron-selector/iron-selection.html","e8d750d36e3375e5a4b5fd0505e7d356"],["bower_components/iron-selector/iron-selector.html","54d8dbf04d1019172a9d824e51215b36"],["bower_components/iron-validatable-behavior/iron-validatable-behavior.html","bcb2eeb6efa393ec07a157276af560f2"],["bower_components/neon-animation/animations/fade-in-animation.html","411879fd2b76d6179d7392a54ab00207"],["bower_components/neon-animation/animations/fade-out-animation.html","2294d54696d1d5d4659e719d4e1cb7a5"],["bower_components/neon-animation/animations/scale-up-animation.html","99fabc596f6ba5125181c78a4322b24f"],["bower_components/neon-animation/neon-animatable-behavior.html","b337b30834e00439ac132f79cf7c6dcf"],["bower_components/neon-animation/neon-animation-behavior.html","0838b9ac437a2da27d21594d52cb9482"],["bower_components/neon-animation/neon-animation-runner-behavior.html","92f4bd3f03e90fe70c551e50b2b8daae"],["bower_components/neon-animation/web-animations.html","e00094c62e90e2f8aea82a6eb3fb656c"],["bower_components/paper-behaviors/paper-button-behavior.html","71633d947c4927a030f837ba079efc1d"],["bower_components/paper-behaviors/paper-checked-element-behavior.html","0ab4c73be42f59e5dda84198d66f8888"],["bower_components/paper-behaviors/paper-inky-focus-behavior.html","9936659cea2f8ef9257da4dbe51b403d"],["bower_components/paper-behaviors/paper-ripple-behavior.html","40eecc31bff39f8e604d9655a37609bf"],["bower_components/paper-button/paper-button.html","80b95e3404a6ac8c695891c47107608b"],["bower_components/paper-card/paper-card.html","aa4cdd9ea4df330a9a39ed89c8caf118"],["bower_components/paper-checkbox/paper-checkbox.html","403440da586f876710b81fc54ffd4cf4"],["bower_components/paper-chip/paper-chip-icons.html","f19454e37317cd28bd7c139ad98b680d"],["bower_components/paper-chip/paper-chip-input.html","9bdbc8e0a81b0a37d8661a901f11ea7e"],["bower_components/paper-chip/paper-chip.html","1480b8d86974d233640b2d7008dd0596"],["bower_components/paper-dialog-behavior/paper-dialog-behavior.html","d4c786ce7b86c86e078bc47a7e5d8121"],["bower_components/paper-dialog-behavior/paper-dialog-shared-styles.html","163bb8092ae2ee96fd07e962ff4d098b"],["bower_components/paper-dialog-scrollable/paper-dialog-scrollable.html","18abbbe67f580c8585c42c71e289aa28"],["bower_components/paper-dialog/paper-dialog.html","95d9c474742b6d22b908e1b873a5642e"],["bower_components/paper-fab/paper-fab.html","508c34b5427e1504ba757032980aec70"],["bower_components/paper-icon-button/paper-icon-button.html","d394d1afbe1448f8c3feb6f8625701ed"],["bower_components/paper-input/paper-input-addon-behavior.html","4a5b44a0688e4ebdbe2e9485c7f61d90"],["bower_components/paper-input/paper-input-behavior.html","e51946bcf9901e279a9185d29f772f77"],["bower_components/paper-input/paper-input-char-counter.html","d0d2b8581663aa58450f891bbe3292d6"],["bower_components/paper-input/paper-input-container.html","8a39df76d592a98421c62c854c7c29c5"],["bower_components/paper-input/paper-input-error.html","cc937dc414267a0c4f599b5ee7ed000a"],["bower_components/paper-input/paper-input.html","4f84144a156313f3f83bdae537a192f4"],["bower_components/paper-input/paper-textarea.html","d31b56af8c1a5d83cc33794d9193033f"],["bower_components/paper-item/paper-icon-item.html","f7bd027def1f297ffc2345bf2939bd1c"],["bower_components/paper-item/paper-item-behavior.html","d165433ff112e8af4197c4a2c43afea5"],["bower_components/paper-item/paper-item-body.html","7b3c5316d0fbb97d256e836a5f01ccc8"],["bower_components/paper-item/paper-item-shared-styles.html","f95e20e03b3edea2d1595e3254b3bebe"],["bower_components/paper-item/paper-item.html","00d0952e52c4b5dad55ca501c2dcf9f1"],["bower_components/paper-listbox/paper-listbox.html","982bcf4dcc90660d5a4d6e0d35e7efb0"],["bower_components/paper-material/paper-material-shared-styles.html","56b1fcfe593f2e9f3bbd2e28d2478bbf"],["bower_components/paper-material/paper-material.html","f80c4886197ea109d8446b9bf3c7f8fd"],["bower_components/paper-menu-button/paper-menu-button-animations.html","fb90d8e5d89da48cb7d684a9a99c2db7"],["bower_components/paper-menu-button/paper-menu-button.html","5a86386842c6487dd4244419ea7028fe"],["bower_components/paper-progress/paper-progress.html","b198caa9c4f0f215b181daca732ab8e8"],["bower_components/paper-ripple/paper-ripple.html","c256da576a77e730e10f97c38f00e217"],["bower_components/paper-spinner/paper-spinner-behavior.html","e5e172bd60b3c579b10992d9e3cc0a13"],["bower_components/paper-spinner/paper-spinner-lite.html","a1bb9114f1a19ac29a6a2b8fbf9c4fc4"],["bower_components/paper-spinner/paper-spinner-styles.html","f6b2d42a9d2262fafb034ea0f802fc80"],["bower_components/paper-styles/color.html","147cdd27d9cf5d6d266a48e0a36cd7fb"],["bower_components/paper-styles/default-theme.html","319cfc7da6fa3aad7f446ecd298d4a3d"],["bower_components/paper-styles/element-styles/paper-material-styles.html","546162960a92790189a80170b01c215b"],["bower_components/paper-styles/paper-styles.html","7a4b409cbbcbc5a2d1bf8cdded888b16"],["bower_components/paper-styles/shadow.html","fe0eb2ec456b10a1866781ddd1e32f06"],["bower_components/paper-styles/typography.html","5177b2dbfe7618814358de5622a7932a"],["bower_components/paper-tabs/paper-tab.html","82066d00bfcd742585b6378302cc4adb"],["bower_components/paper-tabs/paper-tabs-icons.html","2f6c5f81b73f4df9820a406c3266c85b"],["bower_components/paper-tabs/paper-tabs.html","d05d4dc86d8e5fe933205dc09a5a2955"],["bower_components/paper-tooltip/paper-tooltip.html","fc7df54978e86368b251a39d3aab10a2"],["bower_components/polymer/lib/elements/array-selector.html","e7e26dd73ecf8912bfdb88b2eb70f90d"],["bower_components/polymer/lib/elements/custom-style.html","720f086ac98a28ddc14d44e3e45779d8"],["bower_components/polymer/lib/elements/dom-bind.html","81f4ab207cc64282c1478c8097107877"],["bower_components/polymer/lib/elements/dom-if.html","b026cbff8e3a663bbf75163e61f69fe8"],["bower_components/polymer/lib/elements/dom-module.html","5da507765615f5c123d0efd6c0ee2b26"],["bower_components/polymer/lib/elements/dom-repeat.html","77d0cd87823142cd2b157d174575ec1f"],["bower_components/polymer/lib/legacy/class.html","c005310cc036828775ec32f7dba19658"],["bower_components/polymer/lib/legacy/legacy-element-mixin.html","e901d53570a600b604fb97d426077e2c"],["bower_components/polymer/lib/legacy/mutable-data-behavior.html","cec98fd006fb67352afd5dacace36a14"],["bower_components/polymer/lib/legacy/polymer-fn.html","dec6c326b0a860128966f772fe603a52"],["bower_components/polymer/lib/legacy/polymer.dom.html","a7e68ae374c22b360cd02e2a19200a57"],["bower_components/polymer/lib/legacy/templatizer-behavior.html","75547cad8a2b3f6a784735e452f9801b"],["bower_components/polymer/lib/mixins/dir-mixin.html","b427a0cb9a220840808c6033acde95d8"],["bower_components/polymer/lib/mixins/element-mixin.html","3f0bb1fefa6a5239e40fc236d807db06"],["bower_components/polymer/lib/mixins/gesture-event-listeners.html","aa194baaa4981ee8a6ee8d0fedb1c874"],["bower_components/polymer/lib/mixins/mutable-data.html","51f5e1269ca776739a59d2f7440ba0f2"],["bower_components/polymer/lib/mixins/properties-changed.html","f1078f3d839bd22d5d2c4f75b2db9262"],["bower_components/polymer/lib/mixins/properties-mixin.html","94db2a28306dd549d3fe2b9f36e74b15"],["bower_components/polymer/lib/mixins/property-accessors.html","3ae546b3ad5727a9337b2d6a5b7b1c9a"],["bower_components/polymer/lib/mixins/property-effects.html","f7b836008eb9b962f0613c350dee925e"],["bower_components/polymer/lib/mixins/template-stamp.html","2eb71f1f90a4ddb27e31abb407d63363"],["bower_components/polymer/lib/utils/array-splice.html","ed2dff64e9ee2459f197c4b5dfa40d55"],["bower_components/polymer/lib/utils/async.html","e607ddc92613038687147318feba7a25"],["bower_components/polymer/lib/utils/boot.html","844f4d10f0ad2582ea15a01daa461a61"],["bower_components/polymer/lib/utils/case-map.html","3688b5ebabbe0f08a45d3041d15992d7"],["bower_components/polymer/lib/utils/debounce.html","15487e936eb37101e328bc4ea01733f7"],["bower_components/polymer/lib/utils/flattened-nodes-observer.html","d70a18e468cb1b856ab4e90a8b40c66a"],["bower_components/polymer/lib/utils/flush.html","816191b9a81240311f51d0a02ac54fbe"],["bower_components/polymer/lib/utils/gestures.html","1af196669aea9cab9488144c66e020fa"],["bower_components/polymer/lib/utils/html-tag.html","db4283ba6193df958b3d0c8fa54ed147"],["bower_components/polymer/lib/utils/import-href.html","d235b50f7364ad24853e388c0e47235a"],["bower_components/polymer/lib/utils/mixin.html","ca3a32aca09b6135bd17636d93b649cf"],["bower_components/polymer/lib/utils/path.html","5ce25fdab968f4c908a04b457059589d"],["bower_components/polymer/lib/utils/render-status.html","92d5cab79f72fe11c7dfe9f503f58e09"],["bower_components/polymer/lib/utils/resolve-url.html","17c2ea102916e990c83f1530fc8d7738"],["bower_components/polymer/lib/utils/settings.html","28ae919835cc82418c8bc46b00ba0eb0"],["bower_components/polymer/lib/utils/style-gather.html","ced6259bf26e382a9e3921dd736095e6"],["bower_components/polymer/lib/utils/templatize.html","d4d7a77a0a65ab448e112ba96bdd937f"],["bower_components/polymer/lib/utils/unresolved.html","2ed3277470301933b1af10d413d8c614"],["bower_components/polymer/polymer-element.html","c6629b1359c2d3d2eb610a4521bd5ba6"],["bower_components/polymer/polymer.html","f8f1903afaac9dae63ae725d9e98e5bb"],["bower_components/shadycss/apply-shim.html","2e5a3481f6435d48d940ed95d3c132d8"],["bower_components/shadycss/apply-shim.min.js","996204e3caf0fd21a4d0b39bd4cbab17"],["bower_components/shadycss/custom-style-interface.html","c4fadb710b12a7cb3b28d379705b0167"],["bower_components/shadycss/custom-style-interface.min.js","6e2cb1745040846fe648378e542eeb62"],["bower_components/vaadin-checkbox/vaadin-checkbox.html","26c5c13a1a090d410133d9a318a3080a"],["bower_components/vaadin-control-state-mixin/vaadin-control-state-mixin.html","86420d23681d838fac9b87b2de03b443"],["bower_components/vaadin-grid/all-imports.html","612526934f690b0f7854ed80b1c54dc1"],["bower_components/vaadin-grid/iron-list.html","b202febc9880c4888deeda0877735772"],["bower_components/vaadin-grid/vaadin-grid-a11y-mixin.html","fff69a6a61d483897518f411c391d6b6"],["bower_components/vaadin-grid/vaadin-grid-active-item-mixin.html","7c339684bfa1bcea21828973aaa53447"],["bower_components/vaadin-grid/vaadin-grid-array-data-provider-mixin.html","f1ebaeba53fcc1b658da5361e466af17"],["bower_components/vaadin-grid/vaadin-grid-cell-click-mixin.html","bb62d1b2d36b0d045d5392bba85b40cc"],["bower_components/vaadin-grid/vaadin-grid-column-group.html","7386594fc1fbfc71d8d99d7b85b0a66c"],["bower_components/vaadin-grid/vaadin-grid-column-reordering-mixin.html","415c79bb95ce654abf4b7a60a2755dd7"],["bower_components/vaadin-grid/vaadin-grid-column-resizing-mixin.html","d813e138fdf70c6887fbe964a5d4ddb8"],["bower_components/vaadin-grid/vaadin-grid-column.html","e60a99914bd4f2d64e2e77453300d10d"],["bower_components/vaadin-grid/vaadin-grid-data-provider-mixin.html","a1eeceab6157f3625e861bfb14e13122"],["bower_components/vaadin-grid/vaadin-grid-dynamic-columns-mixin.html","4aafb49781f06a9e90bf419f03a8c717"],["bower_components/vaadin-grid/vaadin-grid-filter-mixin.html","676eb043ee65570c1e88ae21b20c18f1"],["bower_components/vaadin-grid/vaadin-grid-filter.html","be53bc7016c234bab09db8954da5248c"],["bower_components/vaadin-grid/vaadin-grid-keyboard-navigation-mixin.html","857c3d7eb6b8407986b605a1ef31901a"],["bower_components/vaadin-grid/vaadin-grid-outer-scroller.html","568041baeccc2f064cdfbe479dd73f45"],["bower_components/vaadin-grid/vaadin-grid-row-details-mixin.html","b3a8f1fff8db1b609f7da04a522b90b2"],["bower_components/vaadin-grid/vaadin-grid-scroll-mixin.html","0e4b9b5d24e6badd138f01fe96e877ad"],["bower_components/vaadin-grid/vaadin-grid-scroller.html","88ae2d2ba0b9993585ce19ebc0985a6e"],["bower_components/vaadin-grid/vaadin-grid-selection-column.html","205dbe7491a3fa1161affc45c4ecc2d3"],["bower_components/vaadin-grid/vaadin-grid-selection-mixin.html","04506cc5fbbde608a04d24583229d814"],["bower_components/vaadin-grid/vaadin-grid-sort-mixin.html","11fcfcb6e486c09057774b192cee7c33"],["bower_components/vaadin-grid/vaadin-grid-sorter.html","43207beafcaa11d78de57c7b9b7a0311"],["bower_components/vaadin-grid/vaadin-grid-styles.html","8e653d533f2d9cfbafb6d35df9b33001"],["bower_components/vaadin-grid/vaadin-grid-templatizer.html","3e1758aaedea6194a0735f04bf651c11"],["bower_components/vaadin-grid/vaadin-grid-tree-toggle.html","98e7ae44ece5229f75100690df546e9c"],["bower_components/vaadin-grid/vaadin-grid.html","1a252264235530411cf6a674645ccf35"],["bower_components/vaadin-text-field/vaadin-form-element-mixin.html","abafdf9a6d61d982f4ed22d50c850078"],["bower_components/vaadin-text-field/vaadin-text-field-mixin.html","31412317ec2372db875f2baccdd69366"],["bower_components/vaadin-text-field/vaadin-text-field.html","07cc9fc36478d26a1c32b0514f0ae808"],["bower_components/vaadin-themable-mixin/vaadin-themable-mixin.html","66a64c448b4ae3097ca23a109464a9ef"],["bower_components/web-animations-js/web-animations-next-lite.min.js","befa29858423272e72afd1711c999e0e"],["bower_components/webcomponentsjs/custom-elements-es5-adapter.js","a5043c1d0dd16d84558ee6cc2276212e"],["bower_components/webcomponentsjs/gulpfile.js","1aac641003c7d14b266843d632cbf71f"],["bower_components/webcomponentsjs/webcomponents-hi-ce.js","6e70d19aa72bca16779abfadceba8d58"],["bower_components/webcomponentsjs/webcomponents-hi-sd-ce.js","874c3be210adb362d08aaf97bbb3f21b"],["bower_components/webcomponentsjs/webcomponents-hi-sd.js","f1db6505f87f7a8660b566a0540e7e5b"],["bower_components/webcomponentsjs/webcomponents-hi.js","c2270cd6fb0b95ed2f87c6b1c143c94f"],["bower_components/webcomponentsjs/webcomponents-lite.js","7354f6c8fce5789ec22b2dbc045f9d52"],["bower_components/webcomponentsjs/webcomponents-loader.js","f13bbbbf647b7922575a7894367ddaaf"],["bower_components/webcomponentsjs/webcomponents-sd-ce.js","20b8283441ed3da5c6c69c5ea9c208ae"],["index.html","7563cb3f75186d0bef8ea54db9389dbd"],["manifest.json","9d09217d099fc574de7f18ef889de8ab"],["src/mapee-components/diagnostics-skeleton/diagnostics-skeleton.html","b2af5cb9c78e98cc202fd3b221833bdc"],["src/mapee-components/gut-content/gut-content.html","31895df2cd529f56b5fe3da37d22b6be"],["src/mapee-components/next-questions/next-questions.html","0552733ef1bf6f137e2fc73402086e9c"],["src/mapee-components/paper-diagnostic/paper-diagnostic.html","f9f6ccd5700e684eb014ab72b4886343"],["src/my-app.html","2e26f6c297942e86f4cf12beaafff881"],["src/my-icons.html","2b2539643b47e16d116fabc3d3802c89"],["src/my-page-action-map.html","b6c40a211cc5161560c18e86971c4b44"],["src/my-page-control-panel.html","97ca64d4febf308304423c97910fbfc8"],["src/my-page-diagnostics.html","2b10176e09c54069656f0b8284f4431b"],["src/my-page-feedback.html","c1c29dfb8c89640e01c96b8a2f73dd46"],["src/my-page-login.html","4a180138d647e7811843635a04f4ebe3"],["src/my-page-password-recovery.html","bbd6963f107bd658131d9f4badc237cb"],["src/my-page-preload.html","cdba1a854bfbb0e93f2249ed8efe2345"],["src/my-page-profile.html","3dc7107eb8e3f5c86b454aa54ef3aec6"],["src/shared-styles.html","333a1fcd41e9a02c18112c261e3e448b"]];
var cacheName = 'sw-precache-v3--' + (self.registration ? self.registration.scope : '');


var ignoreUrlParametersMatching = [/^utm_/];



var addDirectoryIndex = function (originalUrl, index) {
    var url = new URL(originalUrl);
    if (url.pathname.slice(-1) === '/') {
      url.pathname += index;
    }
    return url.toString();
  };

var cleanResponse = function (originalResponse) {
    // If this is not a redirected response, then we don't have to do anything.
    if (!originalResponse.redirected) {
      return Promise.resolve(originalResponse);
    }

    // Firefox 50 and below doesn't support the Response.body stream, so we may
    // need to read the entire body to memory as a Blob.
    var bodyPromise = 'body' in originalResponse ?
      Promise.resolve(originalResponse.body) :
      originalResponse.blob();

    return bodyPromise.then(function(body) {
      // new Response() is happy when passed either a stream or a Blob.
      return new Response(body, {
        headers: originalResponse.headers,
        status: originalResponse.status,
        statusText: originalResponse.statusText
      });
    });
  };

var createCacheKey = function (originalUrl, paramName, paramValue,
                           dontCacheBustUrlsMatching) {
    // Create a new URL object to avoid modifying originalUrl.
    var url = new URL(originalUrl);

    // If dontCacheBustUrlsMatching is not set, or if we don't have a match,
    // then add in the extra cache-busting URL parameter.
    if (!dontCacheBustUrlsMatching ||
        !(url.pathname.match(dontCacheBustUrlsMatching))) {
      url.search += (url.search ? '&' : '') +
        encodeURIComponent(paramName) + '=' + encodeURIComponent(paramValue);
    }

    return url.toString();
  };

var isPathWhitelisted = function (whitelist, absoluteUrlString) {
    // If the whitelist is empty, then consider all URLs to be whitelisted.
    if (whitelist.length === 0) {
      return true;
    }

    // Otherwise compare each path regex to the path of the URL passed in.
    var path = (new URL(absoluteUrlString)).pathname;
    return whitelist.some(function(whitelistedPathRegex) {
      return path.match(whitelistedPathRegex);
    });
  };

var stripIgnoredUrlParameters = function (originalUrl,
    ignoreUrlParametersMatching) {
    var url = new URL(originalUrl);
    // Remove the hash; see https://github.com/GoogleChrome/sw-precache/issues/290
    url.hash = '';

    url.search = url.search.slice(1) // Exclude initial '?'
      .split('&') // Split into an array of 'key=value' strings
      .map(function(kv) {
        return kv.split('='); // Split each 'key=value' string into a [key, value] array
      })
      .filter(function(kv) {
        return ignoreUrlParametersMatching.every(function(ignoredRegex) {
          return !ignoredRegex.test(kv[0]); // Return true iff the key doesn't match any of the regexes.
        });
      })
      .map(function(kv) {
        return kv.join('='); // Join each [key, value] array into a 'key=value' string
      })
      .join('&'); // Join the array of 'key=value' strings into a string with '&' in between each

    return url.toString();
  };


var hashParamName = '_sw-precache';
var urlsToCacheKeys = new Map(
  precacheConfig.map(function(item) {
    var relativeUrl = item[0];
    var hash = item[1];
    var absoluteUrl = new URL(relativeUrl, self.location);
    var cacheKey = createCacheKey(absoluteUrl, hashParamName, hash, false);
    return [absoluteUrl.toString(), cacheKey];
  })
);

function setOfCachedUrls(cache) {
  return cache.keys().then(function(requests) {
    return requests.map(function(request) {
      return request.url;
    });
  }).then(function(urls) {
    return new Set(urls);
  });
}

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return setOfCachedUrls(cache).then(function(cachedUrls) {
        return Promise.all(
          Array.from(urlsToCacheKeys.values()).map(function(cacheKey) {
            // If we don't have a key matching url in the cache already, add it.
            if (!cachedUrls.has(cacheKey)) {
              var request = new Request(cacheKey, {credentials: 'same-origin'});
              return fetch(request).then(function(response) {
                // Bail out of installation unless we get back a 200 OK for
                // every request.
                if (!response.ok) {
                  throw new Error('Request for ' + cacheKey + ' returned a ' +
                    'response with status ' + response.status);
                }

                return cleanResponse(response).then(function(responseToCache) {
                  return cache.put(cacheKey, responseToCache);
                });
              });
            }
          })
        );
      });
    }).then(function() {
      
      // Force the SW to transition from installing -> active state
      return self.skipWaiting();
      
    })
  );
});

self.addEventListener('activate', function(event) {
  var setOfExpectedUrls = new Set(urlsToCacheKeys.values());

  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.keys().then(function(existingRequests) {
        return Promise.all(
          existingRequests.map(function(existingRequest) {
            if (!setOfExpectedUrls.has(existingRequest.url)) {
              return cache.delete(existingRequest);
            }
          })
        );
      });
    }).then(function() {
      
      return self.clients.claim();
      
    })
  );
});


self.addEventListener('fetch', function(event) {
  if (event.request.method === 'GET') {
    // Should we call event.respondWith() inside this fetch event handler?
    // This needs to be determined synchronously, which will give other fetch
    // handlers a chance to handle the request if need be.
    var shouldRespond;

    // First, remove all the ignored parameters and hash fragment, and see if we
    // have that URL in our cache. If so, great! shouldRespond will be true.
    var url = stripIgnoredUrlParameters(event.request.url, ignoreUrlParametersMatching);
    shouldRespond = urlsToCacheKeys.has(url);

    // If shouldRespond is false, check again, this time with 'index.html'
    // (or whatever the directoryIndex option is set to) at the end.
    var directoryIndex = '';
    if (!shouldRespond && directoryIndex) {
      url = addDirectoryIndex(url, directoryIndex);
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond is still false, check to see if this is a navigation
    // request, and if so, whether the URL matches navigateFallbackWhitelist.
    var navigateFallback = 'index.html';
    if (!shouldRespond &&
        navigateFallback &&
        (event.request.mode === 'navigate') &&
        isPathWhitelisted(["\\/[^\\/\\.]*(\\?|$)"], event.request.url)) {
      url = new URL(navigateFallback, self.location).toString();
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond was set to true at any point, then call
    // event.respondWith(), using the appropriate cache key.
    if (shouldRespond) {
      event.respondWith(
        caches.open(cacheName).then(function(cache) {
          return cache.match(urlsToCacheKeys.get(url)).then(function(response) {
            if (response) {
              return response;
            }
            throw Error('The cached response that was expected is missing.');
          });
        }).catch(function(e) {
          // Fall back to just fetch()ing the request if some unexpected error
          // prevented the cached response from being valid.
          console.warn('Couldn\'t serve response for "%s" from cache: %O', event.request.url, e);
          return fetch(event.request);
        })
      );
    }
  }
});







