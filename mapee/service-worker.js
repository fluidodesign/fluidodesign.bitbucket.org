/**
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

// DO NOT EDIT THIS GENERATED OUTPUT DIRECTLY!
// This file should be overwritten as part of your build process.
// If you need to extend the behavior of the generated service worker, the best approach is to write
// additional code and include it using the importScripts option:
//   https://github.com/GoogleChrome/sw-precache#importscripts-arraystring
//
// Alternatively, it's possible to make changes to the underlying template file and then use that as the
// new base for generating output, via the templateFilePath option:
//   https://github.com/GoogleChrome/sw-precache#templatefilepath-string
//
// If you go that route, make sure that whenever you update your sw-precache dependency, you reconcile any
// changes made to this original template file with your modified copy.

// This generated service worker JavaScript will precache your site's resources.
// The code needs to be saved in a .js file at the top-level of your site, and registered
// from your pages in order to be used. See
// https://github.com/googlechrome/sw-precache/blob/master/demo/app/js/service-worker-registration.js
// for an example of how you can register this script and handle various service worker events.

/* eslint-env worker, serviceworker */
/* eslint-disable indent, no-unused-vars, no-multiple-empty-lines, max-nested-callbacks, space-before-function-paren, quotes, comma-spacing */
'use strict';

var precacheConfig = [["bower_components/app-layout/app-drawer-layout/app-drawer-layout.html","1d59e0ed57fa72c8c192147925eb844c"],["bower_components/app-layout/app-drawer/app-drawer.html","e14029895a8741ac6fde01218be08dbc"],["bower_components/app-layout/app-header-layout/app-header-layout.html","d49aecd0524a516cdd0dc8682aca664d"],["bower_components/app-layout/app-header/app-header.html","5709d05bc48985d695ccb55d74844837"],["bower_components/app-layout/app-layout-behavior/app-layout-behavior.html","68c44a7d0ce56eec5179385ddd1fcad5"],["bower_components/app-layout/app-scroll-effects/app-scroll-effects-behavior.html","de8773b7d74d4397bf623ce90136544a"],["bower_components/app-layout/app-scroll-effects/app-scroll-effects.html","47ef4a1229fe38f7ebb0b846676908c9"],["bower_components/app-layout/app-scroll-effects/effects/blend-background.html","9d611dc2e2a68080603abfc412029892"],["bower_components/app-layout/app-scroll-effects/effects/fade-background.html","68c9a3c4a580c2062803443ef39463c8"],["bower_components/app-layout/app-scroll-effects/effects/material.html","93d85d4f6d42fd57d73fda270f8b8b5d"],["bower_components/app-layout/app-scroll-effects/effects/parallax-background.html","10872917947a78a2d05ed70457d7e6c6"],["bower_components/app-layout/app-scroll-effects/effects/resize-snapped-title.html","c6d97d3f7b0215028fb4f1eb5efabfae"],["bower_components/app-layout/app-scroll-effects/effects/resize-title.html","d5c4b389ef48889f231aca4a96168a52"],["bower_components/app-layout/app-scroll-effects/effects/waterfall.html","af6cf17fbb4f94216eea9d2e6c26a775"],["bower_components/app-layout/app-toolbar/app-toolbar.html","1969068eeac3ed606025f04bf7871282"],["bower_components/app-layout/helpers/helpers.html","80b60701ea1a3dbb06fb361a9f92d9b5"],["bower_components/app-route/app-location.html","8fd3320544adb5e25f032dc9ca593d45"],["bower_components/app-route/app-route-converter-behavior.html","67ec6daf2bbe9f59beecbdd5b863af14"],["bower_components/app-route/app-route.html","9c3a42d3d9282b6ac79b7c6936dbcedc"],["bower_components/app-storage/app-indexeddb-mirror/app-indexeddb-mirror-client.html","db45f3e0ebb1793ab2d93a23d98c0c84"],["bower_components/app-storage/app-indexeddb-mirror/app-indexeddb-mirror.html","bf44841abb63ad2337bfb245b901acb8"],["bower_components/app-storage/app-indexeddb-mirror/common-worker.html","9c85fcf413a3fb6c31269c1cce0f4840"],["bower_components/app-storage/app-localstorage/app-localstorage-document.html","3f9885b45243d5bc4168fdaf9c83a249"],["bower_components/app-storage/app-network-status-behavior.html","a06873ebcb4e55881e862d14a4344fb2"],["bower_components/app-storage/app-storage-behavior.html","f30899aad62846eda52b1db2ea0b41de"],["bower_components/fluido-expansion-panel/fluido-expansion-icons.html","e5a01ddd1782713ef16abb53cca690d4"],["bower_components/fluido-expansion-panel/fluido-expansion-panel.html","580394d905e04ffa72bfbdcd1862e0c1"],["bower_components/fluido-expansion-panel/fluido-expansion-panels.html","b704bbd51191f772038733f340d45759"],["bower_components/fluido-paper-login/fluido-paper-login-icons.html","02cf9dd1d39a0bf7a24196e8179ab046"],["bower_components/fluido-paper-login/fluido-paper-login.html","51e0c8692b64624fa3bb7ac015c14916"],["bower_components/fluido-stepper/fluido-step-label.html","2e3890a3fb07bdf2cc77f6b384d82281"],["bower_components/fluido-stepper/fluido-step.html","d9fa8dc076e7371a95faec93f69f2bda"],["bower_components/fluido-stepper/fluido-stepper-icons.html","fba1a7eca2eb2bc31f8ced28f6ba7341"],["bower_components/fluido-stepper/fluido-stepper.html","d0c506727fde26555d52588eb7f56d95"],["bower_components/font-roboto/roboto.html","3dd603efe9524a774943ee9bf2f51532"],["bower_components/google-chart/charts-loader.html","0e92e6ecf626859c15830172bcf15562"],["bower_components/google-chart/google-chart-loader.html","a826d055b908d3b82bdf5ea66704a162"],["bower_components/google-chart/google-chart-styles.html","0edde08b4464aaf1d7de498898f50dc6"],["bower_components/google-chart/google-chart.html","f1e7bbdcaa5bfbe201896dec5e371095"],["bower_components/iron-a11y-announcer/iron-a11y-announcer.html","1844b46b152179da8a8d2b8a8093f06c"],["bower_components/iron-a11y-keys-behavior/iron-a11y-keys-behavior.html","7e459d599801c582676534c6d03a4b13"],["bower_components/iron-a11y-keys/iron-a11y-keys.html","642826d47ea827c603cd0b636e654bbc"],["bower_components/iron-ajax/iron-ajax.html","95fe5061f1c266ef372b644cf6b1bdc6"],["bower_components/iron-ajax/iron-request.html","326a32ef968831b0906376dc79f767fe"],["bower_components/iron-autogrow-textarea/iron-autogrow-textarea.html","b3afd95d90d3d4b643bc4be24a2f7633"],["bower_components/iron-behaviors/iron-button-state.html","7f7f96935de5deaf9a51264225eb1a5a"],["bower_components/iron-behaviors/iron-control-state.html","f1329af310a186a0c3ce264937c34c5e"],["bower_components/iron-checked-element-behavior/iron-checked-element-behavior.html","9ce917fa978d3e488b33ef5183bc6631"],["bower_components/iron-collapse/iron-collapse.html","2b64aae42f66a091d2bba7cf25472166"],["bower_components/iron-dropdown/iron-dropdown-scroll-manager.html","48ae64b446d2f6b9a190e808e76e3caa"],["bower_components/iron-dropdown/iron-dropdown.html","5e69466c8603a435c687e5433887ee2c"],["bower_components/iron-fit-behavior/iron-fit-behavior.html","92bb426360aac378afd1aeac4b055098"],["bower_components/iron-flex-layout/iron-flex-layout.html","03e6f060e1a174a51cc599efac9de802"],["bower_components/iron-form-element-behavior/iron-form-element-behavior.html","2f0a609a52c3b90dc78d529858f04445"],["bower_components/iron-form/iron-form.html","d6701dba3712e61af96e3b00b0c548f6"],["bower_components/iron-icon/iron-icon.html","0d81dc84af38dfdaa7eca375ab7a9b9e"],["bower_components/iron-icons/iron-icons.html","f167b940536136378cba6ddbc6bb00d0"],["bower_components/iron-iconset-svg/iron-iconset-svg.html","b9c649316ce30b30b93f5f96f75d0daf"],["bower_components/iron-image/iron-image.html","2c7dd007dfbf8aa1d759cac087063e6b"],["bower_components/iron-input/iron-input.html","117e335561f348f7cc615443df387405"],["bower_components/iron-location/iron-location.html","6a3c96be0c0e35f26f65567f46f44f06"],["bower_components/iron-location/iron-query-params.html","e7bf2e51c290545d9cd87e4805c042b4"],["bower_components/iron-media-query/iron-media-query.html","0082aca119880bf33ce3ffd1fa0e9011"],["bower_components/iron-menu-behavior/iron-menu-behavior.html","7744b8d275a22ea335c23e4db0012281"],["bower_components/iron-menu-behavior/iron-menubar-behavior.html","300745a77aae1eaa953f015ae1f77025"],["bower_components/iron-meta/iron-meta.html","2e07d1973f152e0b0ea984d98ad5a6ea"],["bower_components/iron-overlay-behavior/iron-focusables-helper.html","a2388bb1967df490e219522e00bb22da"],["bower_components/iron-overlay-behavior/iron-overlay-backdrop.html","b48170aa9276dbdc4a0bc76c3bb65cfe"],["bower_components/iron-overlay-behavior/iron-overlay-behavior.html","2c31da4ab22d8497de3915f945cbf3fb"],["bower_components/iron-overlay-behavior/iron-overlay-manager.html","aa2c1e525807c018b184c53833b18b72"],["bower_components/iron-overlay-behavior/iron-scroll-manager.html","bf24adfcd5aaad2fe8fb4c000e251054"],["bower_components/iron-pages/iron-pages.html","d983c12506f1633551ad7a45537b07e1"],["bower_components/iron-range-behavior/iron-range-behavior.html","562798494651ec5c2a47e7ef0e70d9de"],["bower_components/iron-resizable-behavior/iron-resizable-behavior.html","ef694568c45e136bc268824fd6de7a0a"],["bower_components/iron-scroll-target-behavior/iron-scroll-target-behavior.html","a7b27f01b2f263bf67885d684f9affcc"],["bower_components/iron-selector/iron-multi-selectable.html","2e226f063dd99d8ecda93977d986176b"],["bower_components/iron-selector/iron-selectable.html","c07b54680e6b7cd953bf981a38495402"],["bower_components/iron-selector/iron-selection.html","19a051eb5d88baed09f6439512841bda"],["bower_components/iron-selector/iron-selector.html","76e80b0f3e145257b34de6fde1addd1a"],["bower_components/iron-validatable-behavior/iron-validatable-behavior.html","15574530462b9f0c2ae512b078c596a2"],["bower_components/neon-animation/animations/fade-in-animation.html","036c85fbf438281e2bc9efca073fdf48"],["bower_components/neon-animation/animations/fade-out-animation.html","834a2368655face5daff331858b56d46"],["bower_components/neon-animation/animations/scale-up-animation.html","4547496c1da1bfb8f805638a65252024"],["bower_components/neon-animation/neon-animatable-behavior.html","ca326c00077a9ef323071b2fdab2abd9"],["bower_components/neon-animation/neon-animation-behavior.html","ecc870deadbf9d6d9134a3550eb10676"],["bower_components/neon-animation/neon-animation-runner-behavior.html","dc0200db2837a00660f518bf5f4fa3a6"],["bower_components/neon-animation/web-animations.html","aa5266664b17a9a7d7ebf0c4e6fcf8c9"],["bower_components/paper-behaviors/paper-button-behavior.html","d3c9b2685f6e6585da6cf1e632c50574"],["bower_components/paper-behaviors/paper-checked-element-behavior.html","6bacfe845e0be777b4ae80f02ff85115"],["bower_components/paper-behaviors/paper-inky-focus-behavior.html","52c2ca1ef155e8bca281d806fc9a8673"],["bower_components/paper-behaviors/paper-ripple-behavior.html","d865b73dbb028c24ed30c47da4a3e8fe"],["bower_components/paper-button/paper-button.html","e56a59ed88bb90e19df8338c53e984a5"],["bower_components/paper-card/paper-card.html","86feddb1aa52e5c01a980bd1c0ecc613"],["bower_components/paper-checkbox/paper-checkbox.html","23ff8ddcb1f6ad8a2e3b8f948a85d239"],["bower_components/paper-chip/paper-chip-icons.html","1724d3620a0bdac57f2a285778daf861"],["bower_components/paper-chip/paper-chip-input.html","e298aae770daac2b089d91e0be3eba26"],["bower_components/paper-chip/paper-chip.html","9027c332aba6376bd808a0e9d2e57a92"],["bower_components/paper-dialog-behavior/paper-dialog-behavior.html","c81f9bf9a0173da1dd5af3df073a25cc"],["bower_components/paper-dialog-behavior/paper-dialog-shared-styles.html","583a2b1fd983174e12159eec8a1e5c46"],["bower_components/paper-dialog-scrollable/paper-dialog-scrollable.html","6dbdf7b633a98273c971d15bda39bf11"],["bower_components/paper-dialog/paper-dialog.html","3cdf217af9197ada5462374abda382f2"],["bower_components/paper-fab/paper-fab.html","d2179fce15722c8defad314178fb03d7"],["bower_components/paper-icon-button/paper-icon-button.html","c3acafc40e7feb18eec57b7e49df808c"],["bower_components/paper-input/paper-input-addon-behavior.html","db9171b2bf4fdb8327dd4f311ccc0296"],["bower_components/paper-input/paper-input-behavior.html","de57347c129164c34facff81cd48f54e"],["bower_components/paper-input/paper-input-char-counter.html","515307f96f8507f77b1246b6d17be658"],["bower_components/paper-input/paper-input-container.html","2ab251ffd2f84f0af4f186627ffccb0d"],["bower_components/paper-input/paper-input-error.html","19103517e283f3c553437b1b82a5bcd2"],["bower_components/paper-input/paper-input.html","27d43d4812b2d73eb5e307f6867d395f"],["bower_components/paper-input/paper-textarea.html","44de02c29a3341ecd3805daffc4cb7e9"],["bower_components/paper-item/paper-icon-item.html","e5a5af273afb4448b9f84b3af3cefcd9"],["bower_components/paper-item/paper-item-behavior.html","ccdc3fce427156a1795b26da08a50d06"],["bower_components/paper-item/paper-item-body.html","3102c5ce7c1105b8dc8edfc41d1c4359"],["bower_components/paper-item/paper-item-shared-styles.html","b5104778f1e5f558777d7558623493db"],["bower_components/paper-item/paper-item.html","b81e400f53e1f76d7e2629781773abb3"],["bower_components/paper-listbox/paper-listbox.html","93927bd9e8bbfa08b9d8b8e9d9b66ab8"],["bower_components/paper-material/paper-material-shared-styles.html","0880145bd868df7784d5cd49963468f6"],["bower_components/paper-material/paper-material.html","93846e9b646f5acc9e8e8c45eebb9031"],["bower_components/paper-menu-button/paper-menu-button-animations.html","14091ce3c8f8008b87e0684ff082d514"],["bower_components/paper-menu-button/paper-menu-button.html","d9a9547221b9b4228c4e46e08363c2bd"],["bower_components/paper-progress/paper-progress.html","b04d3e2fed5dcf1060fa9120078873fe"],["bower_components/paper-ripple/paper-ripple.html","0c89f5d6aec27fa86d0a5422dae34099"],["bower_components/paper-spinner/paper-spinner-behavior.html","46ef5ac786242c29920edbff151343cd"],["bower_components/paper-spinner/paper-spinner-lite.html","ca31c3057d0af1d0ccca77134e2f837c"],["bower_components/paper-spinner/paper-spinner-styles.html","7a7e3975c31540dd209e69d7b9cb7ac0"],["bower_components/paper-styles/color.html","549925227bc04f9c17b52e2e35cd2e26"],["bower_components/paper-styles/default-theme.html","5357609d26772a270098c0e3ebb1bb98"],["bower_components/paper-styles/element-styles/paper-material-styles.html","8d8d619e6f98be2c5d7e49ca022e423c"],["bower_components/paper-styles/paper-styles.html","3a86674df8b40032fc42fe95649bbec6"],["bower_components/paper-styles/shadow.html","1f23a65a20ed44812df26a9c16468e3f"],["bower_components/paper-styles/typography.html","195497070df39ff889ce243627cf6589"],["bower_components/paper-tabs/paper-tab.html","bd70d15ac4e9925698696f62f0185961"],["bower_components/paper-tabs/paper-tabs-icons.html","f8e9e4ba00752fc54f1046143ba1be28"],["bower_components/paper-tabs/paper-tabs.html","47db13e0009bb14bbbb382e9b69c33e7"],["bower_components/paper-tooltip/paper-tooltip.html","1d65b9c72461a5eb52718ac6865b3617"],["bower_components/polymer/lib/elements/array-selector.html","52e8ccf3909fdd0f9419e9774d2ca0a7"],["bower_components/polymer/lib/elements/custom-style.html","f40bf2a4b73a468b95ae479828a3dc5a"],["bower_components/polymer/lib/elements/dom-bind.html","0d93f7a399636f6cf6ebad294794304e"],["bower_components/polymer/lib/elements/dom-if.html","42bd24d5b4fb742b2e889bdaf7de0123"],["bower_components/polymer/lib/elements/dom-module.html","5da507765615f5c123d0efd6c0ee2b26"],["bower_components/polymer/lib/elements/dom-repeat.html","bcf3bf90e2e334f1916c24709e49fd80"],["bower_components/polymer/lib/legacy/class.html","4ba6bb406bd899376a4c9af525d92a77"],["bower_components/polymer/lib/legacy/legacy-element-mixin.html","8c64a78f1d58fb00120a651a4383f03f"],["bower_components/polymer/lib/legacy/mutable-data-behavior.html","61308a9cc1b2cd07bdd49037c643f677"],["bower_components/polymer/lib/legacy/polymer-fn.html","4ecb6f82dd2003974ec5004dcb5644f0"],["bower_components/polymer/lib/legacy/polymer.dom.html","15df0f25c67326a90df9ff7ff3144048"],["bower_components/polymer/lib/legacy/templatizer-behavior.html","5a2d1489b25cbcfc0ff535ed9c3b7652"],["bower_components/polymer/lib/mixins/dir-mixin.html","37c54da92010eb75da55564f2a0b6550"],["bower_components/polymer/lib/mixins/element-mixin.html","d4d79d25f307e561336d1ff52cef7037"],["bower_components/polymer/lib/mixins/gesture-event-listeners.html","38c200c539ed88933dbe5bbba675f9a4"],["bower_components/polymer/lib/mixins/mutable-data.html","ae5b34cdf84154794087778b40b70b53"],["bower_components/polymer/lib/mixins/properties-changed.html","f1078f3d839bd22d5d2c4f75b2db9262"],["bower_components/polymer/lib/mixins/properties-mixin.html","fca3e9fb723b1e27b1a56e371a9cd7da"],["bower_components/polymer/lib/mixins/property-accessors.html","fc19ccd4fa69ec0804cb9ec1ac715459"],["bower_components/polymer/lib/mixins/property-effects.html","48bcd690d045c3852b4c527f578ca2a3"],["bower_components/polymer/lib/mixins/template-stamp.html","2eb71f1f90a4ddb27e31abb407d63363"],["bower_components/polymer/lib/utils/array-splice.html","ed2dff64e9ee2459f197c4b5dfa40d55"],["bower_components/polymer/lib/utils/async.html","e607ddc92613038687147318feba7a25"],["bower_components/polymer/lib/utils/boot.html","844f4d10f0ad2582ea15a01daa461a61"],["bower_components/polymer/lib/utils/case-map.html","3688b5ebabbe0f08a45d3041d15992d7"],["bower_components/polymer/lib/utils/debounce.html","15487e936eb37101e328bc4ea01733f7"],["bower_components/polymer/lib/utils/flattened-nodes-observer.html","d70a18e468cb1b856ab4e90a8b40c66a"],["bower_components/polymer/lib/utils/flush.html","816191b9a81240311f51d0a02ac54fbe"],["bower_components/polymer/lib/utils/gestures.html","c03f3ad5bff7c612ea1d63b912529e76"],["bower_components/polymer/lib/utils/html-tag.html","db4283ba6193df958b3d0c8fa54ed147"],["bower_components/polymer/lib/utils/import-href.html","d235b50f7364ad24853e388c0e47235a"],["bower_components/polymer/lib/utils/mixin.html","ca3a32aca09b6135bd17636d93b649cf"],["bower_components/polymer/lib/utils/path.html","5ce25fdab968f4c908a04b457059589d"],["bower_components/polymer/lib/utils/render-status.html","92d5cab79f72fe11c7dfe9f503f58e09"],["bower_components/polymer/lib/utils/resolve-url.html","17c2ea102916e990c83f1530fc8d7738"],["bower_components/polymer/lib/utils/settings.html","28ae919835cc82418c8bc46b00ba0eb0"],["bower_components/polymer/lib/utils/style-gather.html","9a460886072e6590d4e24dbed482dfac"],["bower_components/polymer/lib/utils/templatize.html","05530e70e7630adf7de2e9481073b134"],["bower_components/polymer/lib/utils/unresolved.html","2ed3277470301933b1af10d413d8c614"],["bower_components/polymer/polymer-element.html","929cb2fb73766f6eeabe47f57045c6ca"],["bower_components/polymer/polymer.html","04fe0f988c84c96ecf449ca2381d122d"],["bower_components/shadycss/apply-shim.html","5b73ef5bfcac4955f6c24f55ea322eb1"],["bower_components/shadycss/apply-shim.min.js","996204e3caf0fd21a4d0b39bd4cbab17"],["bower_components/shadycss/custom-style-interface.html","7e28230b85cdcc2488e87172c3395d52"],["bower_components/shadycss/custom-style-interface.min.js","6e2cb1745040846fe648378e542eeb62"],["bower_components/vaadin-checkbox/vaadin-checkbox.html","c158662ac406fc53e1d67ad3826e2b9c"],["bower_components/vaadin-context-menu/vaadin-context-menu.html","5e5da7818ba8f3ddec97e8ee368a5765"],["bower_components/vaadin-context-menu/vaadin-contextmenu-event.html","5d7a425787f3a6f4c5b21dfff7ec2dc2"],["bower_components/vaadin-context-menu/vaadin-device-detector.html","732c6249a679da7e61b1c0e15f71bcb2"],["bower_components/vaadin-control-state-mixin/vaadin-control-state-mixin.html","86420d23681d838fac9b87b2de03b443"],["bower_components/vaadin-grid/all-imports.html","9bc224c6e9623604cfe339b6f8d6ab4b"],["bower_components/vaadin-grid/iron-list.html","1bcf89820cb5a9b076d7f1c0a6a13453"],["bower_components/vaadin-grid/vaadin-grid-a11y-mixin.html","fff69a6a61d483897518f411c391d6b6"],["bower_components/vaadin-grid/vaadin-grid-active-item-mixin.html","7c339684bfa1bcea21828973aaa53447"],["bower_components/vaadin-grid/vaadin-grid-array-data-provider-mixin.html","f1ebaeba53fcc1b658da5361e466af17"],["bower_components/vaadin-grid/vaadin-grid-cell-click-mixin.html","bb62d1b2d36b0d045d5392bba85b40cc"],["bower_components/vaadin-grid/vaadin-grid-column-group.html","ac0629f75ca4019b7b22ff6a4c3a6335"],["bower_components/vaadin-grid/vaadin-grid-column-reordering-mixin.html","325d79ee031ad44806b78164f5cc7d42"],["bower_components/vaadin-grid/vaadin-grid-column-resizing-mixin.html","599dd3042222054a3085b03581aaf59b"],["bower_components/vaadin-grid/vaadin-grid-column.html","8a7d0002efab415bf21d244863b3ccac"],["bower_components/vaadin-grid/vaadin-grid-data-provider-mixin.html","a1eeceab6157f3625e861bfb14e13122"],["bower_components/vaadin-grid/vaadin-grid-dynamic-columns-mixin.html","4aafb49781f06a9e90bf419f03a8c717"],["bower_components/vaadin-grid/vaadin-grid-filter-mixin.html","676eb043ee65570c1e88ae21b20c18f1"],["bower_components/vaadin-grid/vaadin-grid-filter.html","56804b2d623851a02ba330bb568b346c"],["bower_components/vaadin-grid/vaadin-grid-keyboard-navigation-mixin.html","31eb67cc5b34fab7417d47c376bae899"],["bower_components/vaadin-grid/vaadin-grid-outer-scroller.html","0001be319aed23dbd6d4ccb36ac74676"],["bower_components/vaadin-grid/vaadin-grid-row-details-mixin.html","8b1554461795b5dde01b72da113b2fae"],["bower_components/vaadin-grid/vaadin-grid-scroll-mixin.html","0e4b9b5d24e6badd138f01fe96e877ad"],["bower_components/vaadin-grid/vaadin-grid-scroller.html","b9fbb717019a3fc178ad95aab899131d"],["bower_components/vaadin-grid/vaadin-grid-selection-column.html","31ef2b32fc540ac123fe38c2390ccd1e"],["bower_components/vaadin-grid/vaadin-grid-selection-mixin.html","04506cc5fbbde608a04d24583229d814"],["bower_components/vaadin-grid/vaadin-grid-sort-mixin.html","11fcfcb6e486c09057774b192cee7c33"],["bower_components/vaadin-grid/vaadin-grid-sorter.html","8f3da69fc93c69f80df841f714eae1c4"],["bower_components/vaadin-grid/vaadin-grid-styles.html","537f2f80d869e6e229d42ee740268e32"],["bower_components/vaadin-grid/vaadin-grid-templatizer.html","97446a67fa859849f8153dbeaeb3508d"],["bower_components/vaadin-grid/vaadin-grid-tree-toggle.html","c5b845604023481dcaed7638935df286"],["bower_components/vaadin-grid/vaadin-grid.html","f2027cd4de8a8395987ce74a45db8440"],["bower_components/vaadin-overlay/vaadin-overlay.html","f9befd940c80010602ed13e875625fe8"],["bower_components/vaadin-text-field/vaadin-form-element-mixin.html","abafdf9a6d61d982f4ed22d50c850078"],["bower_components/vaadin-text-field/vaadin-text-field-mixin.html","c13f08aa83ea02de0f72259539ad3102"],["bower_components/vaadin-text-field/vaadin-text-field.html","82c54bed7f3689500daf089cb58c8d48"],["bower_components/vaadin-themable-mixin/vaadin-themable-mixin.html","645b05569c610d3ad9c46472f657737d"],["bower_components/web-animations-js/web-animations-next-lite.min.js","befa29858423272e72afd1711c999e0e"],["bower_components/webcomponentsjs/custom-elements-es5-adapter.js","a5043c1d0dd16d84558ee6cc2276212e"],["bower_components/webcomponentsjs/gulpfile.js","1aac641003c7d14b266843d632cbf71f"],["bower_components/webcomponentsjs/webcomponents-hi-ce.js","6e70d19aa72bca16779abfadceba8d58"],["bower_components/webcomponentsjs/webcomponents-hi-sd-ce.js","874c3be210adb362d08aaf97bbb3f21b"],["bower_components/webcomponentsjs/webcomponents-hi-sd.js","f1db6505f87f7a8660b566a0540e7e5b"],["bower_components/webcomponentsjs/webcomponents-hi.js","c2270cd6fb0b95ed2f87c6b1c143c94f"],["bower_components/webcomponentsjs/webcomponents-lite.js","7354f6c8fce5789ec22b2dbc045f9d52"],["bower_components/webcomponentsjs/webcomponents-loader.js","f13bbbbf647b7922575a7894367ddaaf"],["bower_components/webcomponentsjs/webcomponents-sd-ce.js","20b8283441ed3da5c6c69c5ea9c208ae"],["index.html","d8fc53447de09ef35490a38abd319322"],["manifest.json","9d09217d099fc574de7f18ef889de8ab"],["src/mapee-components/diagnostics-skeleton/diagnostics-skeleton.html","ded7a0ab085586b789750969ae74ac6e"],["src/mapee-components/gut-content/gut-content.html","802bacb41a61a90684815b13a6dccc58"],["src/mapee-components/next-questions/next-questions.html","15068fb51b43f7786409000f44c27fe9"],["src/mapee-components/paper-diagnostic/paper-diagnostic.html","2abb95532053ab18af0f88ccef942379"],["src/my-app.html","47b1ad02a37a9e976ee42b475e6130b5"],["src/my-icons.html","20d8f64e91a9d95d169663c6198a9848"],["src/my-page-action-map.html","a1cc4e8cf9eda78ca414eefaab292cc4"],["src/my-page-clients.html","079b89ca96e7d8f5f9d4b1d024253eea"],["src/my-page-control-panel.html","5413fabfdc85a3552a86387fd33e070b"],["src/my-page-diagnostics.html","6aa89d7de5a31cd29476223c358c5b43"],["src/my-page-feedback.html","9fc77dae512fdd4dbb99acce9bcfe0e8"],["src/my-page-login.html","763f576d66ebd21ee62c25dd69088298"],["src/my-page-password-recovery.html","1bce032e00b4fce8f5fe25591460e81a"],["src/my-page-preload.html","38e9587a6e456e74827ad04d777a7aa0"],["src/my-page-profile.html","040fa09a7c2e61c461786ad83241f925"],["src/my-view404.html","58118d81217ef474419293cccbe17cd4"],["src/shared-styles.html","0ce7247a1f7a9a4abee4a7306bc938ed"]];
var cacheName = 'sw-precache-v3--' + (self.registration ? self.registration.scope : '');


var ignoreUrlParametersMatching = [/^utm_/];



var addDirectoryIndex = function (originalUrl, index) {
    var url = new URL(originalUrl);
    if (url.pathname.slice(-1) === '/') {
      url.pathname += index;
    }
    return url.toString();
  };

var cleanResponse = function (originalResponse) {
    // If this is not a redirected response, then we don't have to do anything.
    if (!originalResponse.redirected) {
      return Promise.resolve(originalResponse);
    }

    // Firefox 50 and below doesn't support the Response.body stream, so we may
    // need to read the entire body to memory as a Blob.
    var bodyPromise = 'body' in originalResponse ?
      Promise.resolve(originalResponse.body) :
      originalResponse.blob();

    return bodyPromise.then(function(body) {
      // new Response() is happy when passed either a stream or a Blob.
      return new Response(body, {
        headers: originalResponse.headers,
        status: originalResponse.status,
        statusText: originalResponse.statusText
      });
    });
  };

var createCacheKey = function (originalUrl, paramName, paramValue,
                           dontCacheBustUrlsMatching) {
    // Create a new URL object to avoid modifying originalUrl.
    var url = new URL(originalUrl);

    // If dontCacheBustUrlsMatching is not set, or if we don't have a match,
    // then add in the extra cache-busting URL parameter.
    if (!dontCacheBustUrlsMatching ||
        !(url.pathname.match(dontCacheBustUrlsMatching))) {
      url.search += (url.search ? '&' : '') +
        encodeURIComponent(paramName) + '=' + encodeURIComponent(paramValue);
    }

    return url.toString();
  };

var isPathWhitelisted = function (whitelist, absoluteUrlString) {
    // If the whitelist is empty, then consider all URLs to be whitelisted.
    if (whitelist.length === 0) {
      return true;
    }

    // Otherwise compare each path regex to the path of the URL passed in.
    var path = (new URL(absoluteUrlString)).pathname;
    return whitelist.some(function(whitelistedPathRegex) {
      return path.match(whitelistedPathRegex);
    });
  };

var stripIgnoredUrlParameters = function (originalUrl,
    ignoreUrlParametersMatching) {
    var url = new URL(originalUrl);
    // Remove the hash; see https://github.com/GoogleChrome/sw-precache/issues/290
    url.hash = '';

    url.search = url.search.slice(1) // Exclude initial '?'
      .split('&') // Split into an array of 'key=value' strings
      .map(function(kv) {
        return kv.split('='); // Split each 'key=value' string into a [key, value] array
      })
      .filter(function(kv) {
        return ignoreUrlParametersMatching.every(function(ignoredRegex) {
          return !ignoredRegex.test(kv[0]); // Return true iff the key doesn't match any of the regexes.
        });
      })
      .map(function(kv) {
        return kv.join('='); // Join each [key, value] array into a 'key=value' string
      })
      .join('&'); // Join the array of 'key=value' strings into a string with '&' in between each

    return url.toString();
  };


var hashParamName = '_sw-precache';
var urlsToCacheKeys = new Map(
  precacheConfig.map(function(item) {
    var relativeUrl = item[0];
    var hash = item[1];
    var absoluteUrl = new URL(relativeUrl, self.location);
    var cacheKey = createCacheKey(absoluteUrl, hashParamName, hash, false);
    return [absoluteUrl.toString(), cacheKey];
  })
);

function setOfCachedUrls(cache) {
  return cache.keys().then(function(requests) {
    return requests.map(function(request) {
      return request.url;
    });
  }).then(function(urls) {
    return new Set(urls);
  });
}

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return setOfCachedUrls(cache).then(function(cachedUrls) {
        return Promise.all(
          Array.from(urlsToCacheKeys.values()).map(function(cacheKey) {
            // If we don't have a key matching url in the cache already, add it.
            if (!cachedUrls.has(cacheKey)) {
              var request = new Request(cacheKey, {credentials: 'same-origin'});
              return fetch(request).then(function(response) {
                // Bail out of installation unless we get back a 200 OK for
                // every request.
                if (!response.ok) {
                  throw new Error('Request for ' + cacheKey + ' returned a ' +
                    'response with status ' + response.status);
                }

                return cleanResponse(response).then(function(responseToCache) {
                  return cache.put(cacheKey, responseToCache);
                });
              });
            }
          })
        );
      });
    }).then(function() {
      
      // Force the SW to transition from installing -> active state
      return self.skipWaiting();
      
    })
  );
});

self.addEventListener('activate', function(event) {
  var setOfExpectedUrls = new Set(urlsToCacheKeys.values());

  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.keys().then(function(existingRequests) {
        return Promise.all(
          existingRequests.map(function(existingRequest) {
            if (!setOfExpectedUrls.has(existingRequest.url)) {
              return cache.delete(existingRequest);
            }
          })
        );
      });
    }).then(function() {
      
      return self.clients.claim();
      
    })
  );
});


self.addEventListener('fetch', function(event) {
  if (event.request.method === 'GET') {
    // Should we call event.respondWith() inside this fetch event handler?
    // This needs to be determined synchronously, which will give other fetch
    // handlers a chance to handle the request if need be.
    var shouldRespond;

    // First, remove all the ignored parameters and hash fragment, and see if we
    // have that URL in our cache. If so, great! shouldRespond will be true.
    var url = stripIgnoredUrlParameters(event.request.url, ignoreUrlParametersMatching);
    shouldRespond = urlsToCacheKeys.has(url);

    // If shouldRespond is false, check again, this time with 'index.html'
    // (or whatever the directoryIndex option is set to) at the end.
    var directoryIndex = '';
    if (!shouldRespond && directoryIndex) {
      url = addDirectoryIndex(url, directoryIndex);
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond is still false, check to see if this is a navigation
    // request, and if so, whether the URL matches navigateFallbackWhitelist.
    var navigateFallback = 'index.html';
    if (!shouldRespond &&
        navigateFallback &&
        (event.request.mode === 'navigate') &&
        isPathWhitelisted(["\\/[^\\/\\.]*(\\?|$)"], event.request.url)) {
      url = new URL(navigateFallback, self.location).toString();
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond was set to true at any point, then call
    // event.respondWith(), using the appropriate cache key.
    if (shouldRespond) {
      event.respondWith(
        caches.open(cacheName).then(function(cache) {
          return cache.match(urlsToCacheKeys.get(url)).then(function(response) {
            if (response) {
              return response;
            }
            throw Error('The cached response that was expected is missing.');
          });
        }).catch(function(e) {
          // Fall back to just fetch()ing the request if some unexpected error
          // prevented the cached response from being valid.
          console.warn('Couldn\'t serve response for "%s" from cache: %O', event.request.url, e);
          return fetch(event.request);
        })
      );
    }
  }
});







